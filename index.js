const http = require('http');
const fs = require('fs').promises;
const {v4:uuidv4 } = require('uuid');

const port = 8080;

const server = http.createServer( (req, res) => {

    if(req.method === 'GET') {

        if( req.url === '/html') {

            const htmlFile = 'htmlCode.html';

            fs.readFile(htmlFile, 'utf8')
            .then((data) => {

                res.writeHead(200, {'Content-Type': 'html/text'});
                res.end(data);

            })
            .catch( (err) => {

                res.end(err);

            })

        }else if(req.url === '/json'){

            const jsonFile = './data.json'

            fs.readFile(jsonFile, 'utf8')
            .then( (data) => {
                const content = JSON.parse(data);

                res.writeHead(200, {'Content-Type': 'application/json'});
                res.end(JSON.stringify(content));

            })
            .catch( (err) => {

                res.end(err);

            })
            
        }else if(req.url === '/uuid') {

            const generatedUuid = uuidv4();

            const uuidJson = { "uuid" : generatedUuid};
            res.writeHead(200, {'Content-Type': 'application/json'});

            res.end(JSON.stringify(uuidJson));

        }else if(req.url.startsWith('/status')){
            
            let urlArr = req.url.split('/');
            let statusCode = urlArr[urlArr.length - 1]; 

            res.writeHead(200, {'Content-Type' : 'text/plain'});
            res.end(`${statusCode}`);

        }else if(req.url.startsWith('/delay')){

            let urlArr = req.url.split('/');
            let statusCode = parseInt(urlArr[urlArr.length - 1]);
            
            if(Number.isInteger(statusCode)) {

                setTimeout( () => {

                    res.writeHead(200, {'Content-Type' : 'text/plain'});
                    res.end(`Success response is sending after ${statusCode} seconds`)

                }, statusCode*1000);
            
            }else{

                res.end('Not a number');

            }

        }else{

            let display = { 
                "Try" :         "Below links",
                "html":         `http://localhost:${port}/html`,
                "json":         `http://localhost:${port}/json`,
                "uuid":         `http://localhost:${port}/uuid`,
                "statusCode":   `http://localhost:${port}/status/100`,
                "delay":        `http://localhost:${port}/delay/3`
            }

            res.writeHead(404, { 'Content-Type': 'text/plain' });
            res.end(JSON.stringify(display));

        }
        
    }else{

        res.writeHead(404, { 'Content-Type': 'text/plain' });
        res.end('Method is not found');

    }

});


server.listen(port, () => {
    console.log(`Server running at http://localhost:${port}/`);
});